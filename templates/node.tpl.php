<?php

/**
 * @file
 * Modernist's theme implementation to display a node.
 *
 * Used variables:
 *
 * - $node_id:
 * - $node_classes: A set of CSS classes.
 * - $node_url: Direct url of the current node.
 * - $title: The (sanitized) title of the node.
 * - $submitted: The themed submission information.
 * - $content: Node body or teaser depending.
 * - $links: The themed list of links.
 * - $terms: The themed list of taxonomy term links.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 */
?>
<div id="<?php print $node_id; ?>" class="<?php print $node_classes; ?> clearfix">
  <?php if (!$page): ?>
    <h2><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
  <?php endif; ?>
  <?php if ($submitted): ?>
    <p class="submitted"><?php print $submitted ?></p>
  <?php endif; ?>
  <div class="content">
    <?php print $content ?>
  </div>
  <div class="meta">
    <?php if ($links): ?>
      <div class="links"><?php print $links ?></div>
    <?php endif;?>
    <?php if ($terms): ?>
      <div class="terms"><?php print $terms ?></div>
    <?php endif;?>
  </div>
</div>
