<?php

/**
 * @file
 * Modernist's theme implementation to display a single Drupal page.
 *
 * Used variables:
 *
 * Page metadata:
 * - $language->language: The language the site is being displayed in.
 * - $language->dir: The language direction the site is being displayed in.
 * - $head: Markup for the HEAD element.
 * - $head_title: A modified version of the page title.
 * - $styles: Style tags necessary to import all CSS files for the page.
 * - $styles_ie: Style tags necessary to import conditional CSS files.
 * - $scripts: Script tags necessary to load the JavaScript files.
 * - $body_classes: A set of CSS classes for the BODY tag.
 *
 * Site identity:
 * - $front_page_url: The URL of the front page.
 * - $front_page_title: The title of the front page.
 * - $logo: The path to the logo image.
 * - $logo_title: The title of the logo.
 * - $site_name: The name of the site.
 * - $site_slogan: The slogan of the site.
 *
 * Navigation:
 * - $navigation_links: Primary navigation links for the site.
 *
 * Page content:
 * - $breadcrumb: The breadcrumb trail for the current page.
 * - $messages: HTML for status and error messages.
 * - $title: The page title, for use in the actual HTML content.
 * - $tabs: Tabs linking to any sub-pages beneath the current page.
 * - $help: Dynamic help text, mostly for admin pages.
 *
 * Footer/closing data:
 * - $credits: A string of credits information.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $closure: Final closing markup from any modules that have altered the page.
 *
 * Regions:
 * - $highlighted: Items for the highlighted content region.
 * - $content: The main content of the current page.
 * - $left: Items for the top sidebar.
 * - $right: Items for the bottom sidebar.
 * - $triptych_first: Items for the first triptych.
 * - $triptych_middle: Items for the middle triptych.
 * - $triptych_last: Items for the last triptych.
 * - $footer: Items for the footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language; ?>" lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>">
  <head>
    <?php print $head ?>
    <title><?php print $head_title ?></title>
    <?php print $styles ?>
    <!--[if lt IE 8]>
      <?php print $styles_ie; ?>
    <![endif]-->
    <?php print $scripts ?>
  </head>
  <body class="<?php print $body_classes; ?>">
    <div id="wrapper">
      <?php if ($navigation_links): ?>
        <div id="navigation" class="clearfix">
          <?php print $navigation_links; ?>
        </div>
      <?php endif; ?>
      <div id="branding" class="clearfix">
        <?php if ($logo): ?>
          <a href="<?php print $front_page_url; ?>" title="<?php print $front_page_title; ?>" rel="home" id="logo"><img src="<?php print $logo; ?>" alt="<?php print $logo_title; ?>" /></a>
        <?php endif; ?>
        <?php if ($site_name || $site_slogan): ?>
          <div id="name-and-slogan">
            <?php if ($site_name): ?>
              <h1 id="site-name"><a href="<?php print $front_page_url; ?>" title="<?php print $front_page_title; ?>" rel="home"><?php print $site_name; ?></a></h1>
            <?php endif; ?>
            <?php if ($site_slogan): ?>
              <p id="site-slogan"><?php print $site_slogan; ?></p>
            <?php endif; ?>
          </div>
        <?php endif; ?>
      </div>
      <div id="main" class="clearfix">
        <div id="content">
          <?php print $breadcrumb; ?>
          <?php print $messages; ?>
          <?php if ($highlighted): ?>
            <div id="highlighted">
              <?php print $highlighted; ?>
            </div>
          <?php endif; ?>
          <?php if ($title): ?>
            <h2 id="page-title"><?php print $title; ?></h2>
          <?php endif; ?>
          <?php if ($tabs): ?>
            <div id="page-tabs"><?php print $tabs; ?></div>
          <?php endif; ?>
          <?php print $help; ?>
          <?php print $content; ?>
        </div>
        <?php if ($left || $right): ?>
          <div id="sidebar">
            <?php if ($left): ?>
              <div id="sidebar-top">
                <?php print $left; ?>
              </div>
            <?php endif; ?>
            <?php if ($right): ?>
              <div id="sidebar-bottom">
                <?php print $right; ?>
              </div>
            <?php endif; ?>
          </div>
        <?php endif; ?>
      </div>
      <?php if ($triptych_first || $triptych_middle || $triptych_last): ?>
        <div id="triptych" class="clearfix">
          <div id="triptych-first">
            <?php print $triptych_first; ?>
          </div>
          <div id="triptych-middle">
            <?php print $triptych_middle; ?>
          </div>
          <div id="triptych-last">
            <?php print $triptych_last; ?>
          </div>
        </div>
      <?php endif; ?>
      <?php if ($footer): ?>
        <div id="footer" class="clearfix">
          <?php print $footer; ?>
        </div>
      <?php endif; ?>
      <div id="credits" class="clearfix">
        <p><?php print $credits; ?> <?php print $feed_icons; ?></p>
      </div>
    </div>
  <?php print $closure; ?>
  </body>
</html>
