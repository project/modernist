<?php

/**
 * @file
 * Theme settings callbacks for the modernist theme.
 */


/**
 * Implementation of THEME_settings().
 */
function modernist_settings($saved_settings) {

  $defaults = array();
  $defaults['color_scheme'] = 'blue-scheme';
  $defaults['breadcrumb'] = array();

  $settings = array_merge($defaults, $saved_settings);

  $form = array();
  $form['color_scheme'] = array(
    '#type' => 'select',
    '#title' => t('Color scheme'),
    '#default_value' => $settings['color_scheme'],
    '#options' => array(
      'blue-scheme' => t('Blue scheme'),
//      'red-scheme' => t('Red scheme'),
    ),
  );
  $form['breadcrumb'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Breadcrumb'),
    '#default_value' => $settings['breadcrumb'],
    '#options' => array(
      'display' => t('Display breadcrumb.'),
    ),
  );

  return $form;
}
