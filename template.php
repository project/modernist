<?php

/**
 * @file
 * Theme callbacks for the modernist theme.
 */


/**
 * Implementation of THEME_preprocess_HOOK.
 */
function modernist_preprocess_page(&$variables) {

  $color_scheme = theme_get_setting('color_scheme');
  $breadcrumb = theme_get_setting('breadcrumb');

  // Page metadata
  $variables['styles_ie'] = '<link type="text/css" rel="stylesheet" media="media" href="'. base_path() . path_to_theme() .'/styles/style-ie.css" />';
  $variables['body_classes'] .= ' '. ($color_scheme ? $color_scheme : 'blue-scheme');

  // Site identity
  $variables['front_page_url'] = check_url($variables['front_page']);
  $variables['front_page_title'] = t('Home');
  $variables['logo_title'] = t('Logo');

  // Navigation
  $variables['navigation_links'] = theme('links', $variables['primary_links']);

  // Page content
  if (empty($breadcrumb['display'])) {
    $variables['breadcrumb'] = NULL;
  }

  // Footer/closing data
  $variables['credits'] = t('Proudly powered by <a href="http://www.drupal.org">Drupal</a> and <a href="http://drupal.org/project/modernist" title="Free Drupal theme">Modernist</a>, a theme by <a href="http://www.rodrigogalindez.com" title="Web Designer">Rodrigo Galindez</a> and <a href="http://www.ralfstamm.com" title="Web coder, consultant, and trainer">Ralf Stamm</a>.');
}


/**
 * Implementation of THEME_preprocess_maintenance_HOOK.
 */
function modernist_preprocess_maintenance_page(&$variables) {

  modernist_preprocess_page($variables);
}


/**
 * Implementation of THEME_preprocess_HOOK.
 */
function modernist_preprocess_node(&$variables) {

  $node = $variables['node'];

  $variables['node_id'] = 'node-'. $node->nid;
  $variables['node_classes'] = 'node';
  $variables['node_classes'] .= ' node-type-'. $node->type;
  $variables['node_classes'] .= $node->sticky ? ' sticky' : '';
  $variables['node_classes'] .= !$node->status ? ' unpublished' : '';
}


/**
 * Implementation of theme_node_submitted.
 */
function modernist_node_submitted($node) {

  return t('by !username on @datetime', array('!username' => theme('username', $node), '@datetime' => format_date($node->created)));
}
