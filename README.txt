

Theme
=====
Modernist


Description
===========
The Modernist theme is a fixed width, two columns, minimal theme focused on
great typography.


Requirements
============
- Drupal 6.x


Installation
============
1. Move this folder into your themes directory.
2. Enable it from Administer >> Site building >> Themes.


Configuration
=============
Configure it at Administer >> Site building >> Themes >> Configure >> Modernist.

How to customize CSS, please read the file styles/custom-default.css.


Projekt page
============
http://drupal.org/project/modernist


Credits
=======
- Designed by Rodrigo Galindez, http://www.rodrigogalindez.com.
- Written and maintained by Ralf Stamm, http://www.ralfstamm.com.


License
=======
GNU General Public License (GPL)
